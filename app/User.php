<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'sc_users';
    //protected $fillable = ['_token'];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'changed_date';
}
