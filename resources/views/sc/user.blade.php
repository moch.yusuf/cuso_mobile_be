   @extends('layouts.master')
   @section('content')



   <!-- Content Header (Page header) -->
   <section class="content-header">
       <div class="container-fluid">
           <div class="row mb-2">
               <div class="col-sm-6">
                   <h1>User Management</h1>
               </div>
               <div class="col-sm-6">
                   <ol class="breadcrumb float-sm-right">
                       <li class="breadcrumb-item"><a href="#">Home</a></li>
                       <li class="breadcrumb-item active">User Management</li>
                   </ol>
               </div>
           </div>
       </div><!-- /.container-fluid -->
   </section>

   <!-- Main content -->
   <section class="content">


       <!-- /.card -->

       <div class="card">
           <div class="card-header">
               <div class="col-lg-12">
                   <div class="row justify-content-end">
                       <div class="pull-right">
                           <a href="{{'users_add'}}" class="btn btn-block btn-info btn-sm"><span class="fas fa-plus"></span> Add User</a>
                       </div>
                   </div>
               </div>
           </div>
           <!-- /.card-header -->
           <div class="card-body">
               <table id="example2" class="table table-bordered table-striped">
                   <thead>
                       <tr>
                           <th>#</th>
                           <th>First Name</th>
                           <th>Last Name</th>
                           <th>User Name</th>
                           <th>Email</th>
                           <th>Action</th>
                       </tr>
                   </thead>
                   <tbody>
                       
                       @foreach($users as $user)

                       <tr>
                           <td style="padding: .2rem">{{$loop->iteration}}</td>
                           <td style="padding: .2rem">{{$user->first_name}}</td>
                           <td style="padding: .2rem">{{$user->last_name}}</td>
                           <td style="padding: .2rem">{{$user->email}}</td>
                           <td style="padding: .2rem">{{$user->user_name}}</td>
                           <td style="padding: .2rem">
                               <a href="user/{{$user->id}}/edit" class="btn btn-xs btn-success btn-xs">Edit</a>
                               <a href="#" class="btn btn-xs btn-danger btn-xs">Delete</a>
                               <a href="#" class="btn btn-xs btn-warning btn-xs">Permission</a>
                           </td>
                       </tr>
                       
                       @endforeach
                   </tbody>
               </table>
           </div>
           <!-- /.card-body -->
       </div>
       <!-- /.card -->


   </section>
   <!-- /.content -->

   @section('scripts')
   <!-- DataTables -->
   <script src="adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
   <script src="adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
   <script src="adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
   <script src="adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

   <!-- page script -->
   <script>
       $(function() {
           $('#example2').DataTable({

           });
       });
   </script>
   @endsection
   @endsection