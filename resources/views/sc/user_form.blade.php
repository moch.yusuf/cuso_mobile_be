 @extends('layouts.master')
 @section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1>User Management</h1>
             </div>
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Home</a></li>
                     <li class="breadcrumb-item active">User Management</li>
                 </ol>
             </div>
         </div>
     </div><!-- /.container-fluid -->
 </section>

 <!-- Main content -->
 <section class="content">
     <!-- general form elements -->
     <div class="card card-info">
         <div class="card-header">

             <h3 class="card-title">{{$tittle}}</h3>
        
         </div>
         <!-- /.card-header -->
         <!-- form start -->

         <form role="form" method="post" action="{{$action_name}}">
             @method('patch')
             @csrf
             <div class="card-body">
                 <div class="row">
                     <div class="col-6">
                         <div class="form-group col-md-10">
                             <label for="FirstName">First Name</label>
                             <input value="{{$user->first_name}}" class="form-control form-control-sm" type="text" name="Name" placeholder="Enter First Name">
                         </div>

                         <div class="form-group col-md-10">
                             <label for="LastName">Last Name</label>
                             <input value="{{$user->last_name}}" class="form-control form-control-sm" type="text" name="LastName" placeholder="Enter Last Name">
                         </div>

                         <div class="form-group col-md-10">
                             <label for="UserName">User Name</label>
                             <input value="{{$user->user_name}}" class="form-control form-control-sm" type="text" name="UserName" placeholder="Enter User Name">
                         </div>

                         <div class="form-group col-md-10">
                             <label for="Email">Email address</label>
                             <input value="{{$user->email}}" class="form-control form-control-sm" type="text" name="Email" placeholder="Enter email">
                         </div>
                     </div>

                     <div class="col-6">
                         <div class="form-group col-10">
                             <label for="Password">Password</label>
                             <input value="{{$user->password}}" class="form-control form-control-sm" type="password" name="Password" placeholder="Password">
                         </div>

                         <div class="form-group col-10">
                             <label for="RePassword">Re-enter Password</label>
                             <input value="{{$user->password}}" class="form-control form-control-sm" type="password" name="RePassword" placeholder="Re-enter Password">
                         </div>
                     </div>
                 </div>
             </div>
             <!-- /.card-body -->

             <div class="card-footer">
                 <div class="row">
                     <div class="col-6">
                         <button type="submit" class="btn btn-info">Submit</button>
                         <a href="/{{'users'}}"><span class="btn btn-info">Cancel</span></a>
                     </div>
                 </div>
             </div>
         </form>


     </div>
     <!-- /.card -->



 </section>
 <!-- /.content -->
 @endsection