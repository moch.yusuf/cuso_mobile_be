<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feature_name');
            $table->string('feature_description')->unique();            
            $table->integer('created_by');
            $table->timestamp('created_date');
            $table->integer('changed_by');
            $table->timestamp('changed_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_features');
    }
}
