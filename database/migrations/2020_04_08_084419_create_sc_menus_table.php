<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_parent_id');
            $table->string('menu_name')->unique();
            $table->string('url');
            $table->string('icon');
            $table->integer('created_by');
            $table->timestamp('created_date');
            $table->integer('changed_by');
            $table->timestamp('changed_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_menus');
    }
}
