<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScMenuFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_menu_features', function (Blueprint $table) {
			$table->increments('id');
            $table->string('menu_id');
            $table->string('feature_id');
            $table->integer('created_by');            
            $table->timestamp('created_date');
            $table->integer('changed_by');
            $table->timestamp('changed_date');	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_menu_features');
    }
}
