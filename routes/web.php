<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('/layouts/master');
});
*/

//Route::group(['middleware' => 'VerifyCsrfToken'], function () {
    Route::post('user', 'UsersController@store');    
//});

Route::get('/', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
//Route::get('/', 'AdminController@dashboard');
//Route::get('dashboard', 'AdminController@dashboard')->middleware('auth');
Route::get('/', 'AdminController@dashboard');
Route::get('users', 'UsersController@index');
Route::get('users_add', 'UsersController@create');
Route::get('user/{user}/edit', 'UsersController@edit');

Route::get('transppob', 'TransPPOBsController@index');
Route::get('transppob/getsaldo', 'TransPPOBsController@getSaldo');


